
let button = document.getElementById('getData');
let area = document.getElementById('dataArea');

button.addEventListener('click', function (event) {
    axios.get('http://localhost:5000/api/users/5')
        .then(function (response) {
            area.innerText = JSON.stringify(response.data);
            console.log(response.data);
        })
        .catch(function (error) {
            console.log(error);
        });
});
