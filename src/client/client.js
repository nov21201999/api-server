

let axios = require('axios');

//------------------------------------------

let config = require('./config.js');

//------------------------------------------

/*
// Make a request for a user with a given ID
axios.get('/holiday/getAllHolidays', config.axiosConfig)
    .then(function (response) {
        console.log(response.data);
    })
    .catch(function (error) {
        console.log(error);
    });

*/


function getUser(user_id = null) {

    let id = (user_id) ? user_id : '';

    axios.get('/users/'+id, config.axiosConfig)
        .then(function (response) {
            console.log('Nicht mehr warten, hier sind die Daten');
            console.log(response.data);
            
        })
        .catch(function (error) {
            console.error(error);
        });

}


console.log('Daten werden abgerufen');
getUser(13);
console.log('Bitte warten');