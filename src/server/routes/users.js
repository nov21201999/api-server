

const mysql = require('mysql');

//-----------------------------------------------

const config = require('../config.js');

//-----------------------------------------------

let connection = mysql.createConnection(config.mysqlConfig);

connection.connect(function (err) {
    if (err) console.log(err);
    else console.log('Linked to Database ' + config.mysqlConfig.database );
});

//-----------------------------------------------




exports.getUser = function(req, res) {

    let userid = req.params.userid;

    if (userid) {
        connection.query('SELECT * FROM user WHERE id = ?', [userid], function (error, results) {
            if (error) {
                console.log(error);
                res.send(500);
            } else {
                res.send(results);
            }
        });
    } else {
        connection.query('SELECT * FROM user', function (error, results) {
            if (error) {
                console.log(error);
                res.send(500);
            } else {
                res.send(results);
            }
        });
    }

};
