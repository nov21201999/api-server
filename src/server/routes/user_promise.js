let mysql = require('promise-mysql');


//-----------------------------------

const config = require('../config.js');

//-----------------------------------


exports.getUser = function(req, res) {

    let userid = req.params.userid;

    mysql.createConnection(config.mysqlConfig).then(function(conn){
        // do stuff with conn

        console.log('Connected to Database '+config.mysqlConfig.database);


        let result = (userid) ?
            conn.query('SELECT * FROM user WHERE id = ?', [userid]) :
            conn.query('SELECT * FROM user WHERE 1');

        // end connection
        conn.end();

        return result;
    }).then(function (result) {
        // do stuff with result

        res.send(result);

    });


};


exports.getAllHolidays = function(req, res) {
    mysql.createConnection(config.mysqlConfig).then(function(conn) {

        let result = conn.query('SELECT * FROM holiday WHERE 1');

        conn.end();

        return result;

    }).then(function(result) {
        res.send(result);
    });
};



