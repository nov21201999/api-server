const express = require('express');
const bodyParser = require('body-parser');

//----------------------------------------------------

const config = require('./config.js');

const routes = require('./routes/users.js');

const routes_promise = require('./routes/user_promise.js');

//----------------------------------------------------

let app = express();


// to support JSON-encoded bodies
app.use( bodyParser.json() );
// to support URL-encoded bodies
app.use( bodyParser.urlencoded({ extended: true }) );

//----------------------------------------------------

// CORS (Cross-Origin Resource Sharing) to support Cross-Site HTTP requests
app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
    res.header("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    next();
});


//----------------------------------------------------


//app.get('/api/users/:userid?', routes.getUser);
app.get('/api/users/:userid?', routes_promise.getUser);

app.get('/api/holiday/getAllHolidays', routes_promise.getAllHolidays);


//----------------------------------------------------

app.set('port', config.expressConfig.port);

app.listen(app.get('port'), function () {
    console.log('Express Server listening on port ' + app.get('port'));
});
