
module.exports = {

    'expressConfig': {
        port : 5000
    },

    'mysqlConfig': {
        host     : 'localhost',
        port     : 3306,
        user     : 'mvc_db',
        password : 'mvc_pw',
        database : 'mvc_db'
    }

};
